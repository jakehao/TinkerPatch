package tinker.sample.android.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by huqs on 2017/1/10.
 */

public class RestartBroadcast extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {

        new Thread(){
            @Override
            public void run() {
                super.run();
                while (true){
                    try {
                        Thread.sleep(100);
                        System.out.println("----------->android.tinkerpatch.app.install");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }
}
